import { Api } from 'api/Api';
import {EntryModel, makeEntryId} from 'model/EntryModel';

export class IndexedDB implements Api {
    private db: IDBDatabase|null = null;

    public fetchEntries(): Promise<Array<EntryModel>> {
        return new Promise((resolve, reject) => {
            this.connect()
            .then(db => {
                const store = db.transaction('entries').objectStore('entries');
                const index = store.index('date');
                const cursorReq = index.openCursor(null, 'prev');

                let result: Array<EntryModel> = [];

                cursorReq.onsuccess = function() {
                    if (this === null) {
                        resolve(result);
                    }

                    if (this.result) {
                        result.push(this.result.value);
                        this.result.continue();
                    } else {
                        resolve(result);
                    }
                };

                cursorReq.onerror = event => {
                    reject(event);
                };
            });
        });
    }

    public saveEntry(entry: EntryModel): Promise<EntryModel> {
        if (entry.id === '') {
            entry.id = makeEntryId();
        }

        return new Promise((resolve, reject) => {
            this.connect()
            .then(db => {
                const store = db
                    .transaction(['entries'], 'readwrite')
                    .objectStore('entries');

                const result = store.put(entry);

                result.onsuccess = () => {
                    resolve(entry);
                };

                result.onerror = () => {
                    reject(result.error);
                };
            });
        });
    }

    public removeEntry(entry: EntryModel): Promise<void> {
        return new Promise((resolve, reject) => {
            this.connect()
            .then(db => {
                const store = db
                    .transaction(['entries'], 'readwrite')
                    .objectStore('entries');

                const result = store.delete(entry.id);

                result.onsuccess = () => {
                    resolve();
                };

                result.onerror = () => {
                    reject(result.error);
                };
            });
        });
    }

    public connected(): boolean {
        return this.db !== null;
    }

    private connect(): Promise<IDBDatabase> {
        return new Promise((resolve, reject) => {
            if (this.db !== null) {
                return resolve(this.db);
            }

            const openRequest = window.indexedDB.open('entries', 2);

            openRequest.onerror = function(event) {
                console.log('Failed to open indexedDB', event);
                reject(event);
            };

            openRequest.onsuccess = () => {
                this.db = openRequest.result;
                resolve(this.db);
            }

            const this_ = this;
            openRequest.onupgradeneeded = function(event) {
                this_.upgrade(this, event);
            }
        });
    }

    private upgrade(req: IDBOpenDBRequest, event: IDBVersionChangeEvent): void {
        const db = req.result;

        db.onerror = event => {
            console.log('Failed while upgrading database', event);
        };

        const store = db.createObjectStore('entries', {keyPath: 'id'});

        store.createIndex("date", "date");
    }
}
