import {EntryModel} from 'model/EntryModel';

export interface Api {
    fetchEntries: () => Promise<Array<EntryModel>>;
    saveEntry: (entry: EntryModel) => Promise<EntryModel>;
    removeEntry: (entry: EntryModel) => Promise<void>;
}
