import { Api } from 'api/Api';
import {EntryModel} from 'model/EntryModel';

export class RestHttp implements Api {
    private rootUrl: string = '/api';

    constructor(rootUrl: string = '/api') {
        this.rootUrl = rootUrl;
    }

    public async fetchEntries(): Promise<Array<EntryModel>> {
        const response = await fetch(`${this.rootUrl}/entries`);

        return await response.json();
    }

    public async saveEntry(entry: EntryModel): Promise<EntryModel> {
        const response = await fetch(`${this.rootUrl}/entries/${entry.id}`, {
            method: 'PUT',
            body: JSON.stringify(entry),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        return await response.json();
    }

    public async removeEntry(entry: EntryModel): Promise<void> {
        await fetch(`${this.rootUrl}/entries/${entry.id}`, {
            method: 'DELETE',
        });
    }
}
