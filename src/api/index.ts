import { RestHttp } from 'api/RestHttp';

const api = new RestHttp(process.env.REACT_APP_API_URL);

export default api;
