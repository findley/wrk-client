import React from 'react';
import ReactDOM from 'react-dom';
import './styles.scss';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// @ts-ignore
window.openIssue = (event: any) => {
    if (!event.ctrlKey) {
        return;
    }

    const tagText: string = event.target.innerText;
    const issueNumber = tagText.substring(1);
    const link = `https://youtrack.paycomhq.com/issues/${issueNumber}`;
    window.open(link, '_newtab');
}
