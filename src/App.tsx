import React, { useReducer, useEffect } from 'react';
import Entry from 'components/entry/Entry';
import { initialState, reducer, saveEntry } from 'store';
import api from './api';

function App() {
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        api.fetchEntries().then(entries => {
            dispatch({type: 'LOADED_ENTRIES', entries});
        });
    }, []);

    const entries = state.entries.map(entry => (
        <Entry
            entry={entry}
            onChange={e => dispatch({type: "UPDATE_ENTRY", entry: e})}
            onFinishEdit={saveEntry(dispatch)}
            key={entry.id}
        />
    ));

    return (
        <div className="container">
            <h1>rwrk</h1>
            <section>{entries}</section>
        </div>
    );
}

export default App;
