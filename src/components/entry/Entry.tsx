import React, {FunctionComponent, useState} from 'react';
import {classList} from 'utils';
import {EntryModel} from 'model/EntryModel';
import TextInput from 'components/entry/text-input/TextInput';

type EntryProps = {
    entry: EntryModel,
    onChange: (entry: EntryModel) => void,
    onFinishEdit: (entry: EntryModel) => void,
};

const Entry: FunctionComponent<EntryProps> = props => {
    const {entry, onChange, onFinishEdit} = props;

    const [focused, setFocused] = useState(false);

    const handleChange = (
        field: string,
        event: React.ChangeEvent<HTMLInputElement|HTMLTextAreaElement>
    ) => {
        onChange({...entry, [field]: event.target.value});
    };

    const handleSubmit = (event: React.FormEvent) => {
        event.preventDefault();
        onFinishEdit(entry);
    };

    const handleFocus = () => {
        setFocused(true);
    };

    const handleBlur = () => {
        setFocused(false);
    };

    const editing = focused || entry.id === '';

    const classes = classList({
        'entry': true,
        'entry-editing': editing,
    });

    return (
        <form className={classes} onSubmit={handleSubmit}>
            <div className="entry-header">
                <input
                    value={entry.subject}
                    onChange={e => handleChange('subject', e)}
                    onFocus={handleFocus}
                    onBlur={handleBlur}
                    name="subject"
                    type="text"
                    placeholder="Subject"
                />
                <input
                    value={entry.date}
                    onChange={e => handleChange('date', e)}
                    onFocus={handleFocus}
                    onBlur={handleBlur}
                    name="date"
                    type="datetime-local"
                    placeholder="Date"
                />
                <input
                    value={entry.duration}
                    onChange={e => handleChange('duration', e)}
                    onFocus={handleFocus}
                    onBlur={handleBlur}
                    name="duration"
                    type="text"
                    placeholder="1h30m"
                />
            </div>

            <TextInput
                value={entry.body}
                editing={editing}
                onChange={e => handleChange('body', e)}
                onFocus={handleFocus}
                onBlur={handleBlur}
                placeholder="Details..."
            />

            <button type="submit" style={{display: 'none'}}></button>
        </form>
    );
};

export default Entry;
