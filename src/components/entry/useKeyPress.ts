import { useState, useEffect } from 'react';

export function useKeyPress(targetKey: string) {
    const [keyPressed, setKeyPressed] = useState(false);

    const downHandler = ({ key }: KeyboardEvent) => {
        if (key === targetKey) {
            setKeyPressed(true);
        }
    }

    const upHandler = ({ key }: KeyboardEvent) => {
        if (key === targetKey) {
            setKeyPressed(false);
        }
    }

    useEffect(() => {
        window.addEventListener('keydown', downHandler);
        window.addEventListener('keyup', upHandler);

        return () => {
            window.removeEventListener('keydown', downHandler);
            window.removeEventListener('keyup', downHandler);
        };
    // eslint think we need to track down/up handlers
    // eslint-disable-next-line
    }, []);

    return keyPressed;
}
