import React, {FunctionComponent, useMemo} from 'react';
import { useKeyPress } from '../useKeyPress';

type TextInputProps = {
    value: string,
    editing: boolean,
    placeholder?: string,
    onFocus?: () => void,
    onBlur?: () => void,
    onChange: (value: React.ChangeEvent<HTMLTextAreaElement>) => void
};

const TextInput: FunctionComponent<TextInputProps> = props => {
    const {placeholder, value, onChange, editing, onFocus, onBlur} = props;
    const ctrlPressed = useKeyPress('Control');
    const highlightedText = useMemo(() => applyHighlights(value, ctrlPressed), [value, ctrlPressed]);

    return (
        <div className="text-input">
            <div dangerouslySetInnerHTML={{__html: highlightedText}}></div>
            <textarea
                onFocus={onFocus}
                onBlur={onBlur}
                value={value}
                onChange={onChange}
                spellCheck={editing}
                placeholder={editing ? placeholder : ''}
            />
        </div>
    );
};

const tagRegex = new RegExp(/(#[a-zA-Z0-9\-_.]+)[\w|$]/g);
const issueRegex = new RegExp(/(![a-zA-Z0-9\-_.]+)[\w|$]/g);
const atRegex = new RegExp(/(@[a-zA-Z0-9\-_.]+)[\w|$]/g);

function applyHighlights(text: string, clickable: boolean): string {
    let issueMark = '<mark class="mark-issue">$&</mark>';
    if (clickable) {
        issueMark = '<mark onclick="openIssue(event)" class="mark-issue mark-clickable">$&</mark>'
    }

    console.log('applyHighlights');
    return text
        .replace(tagRegex, '<mark class="mark-tag">$&</mark>')
        .replace(issueRegex, issueMark)
        .replace(atRegex, '<mark class="mark-at">$&</mark>');
}

export default TextInput;
