import { EntryModel, emptyEntry, isSaveable } from 'model/EntryModel';
import api from 'api/';

export type AppState = {
    entries: Array<EntryModel>
}

type Action =
    | { type: 'UPDATE_ENTRY', entry: EntryModel }
    | { type: 'REMOVE_ENTRY', entry: EntryModel }
    | { type: 'LOADED_ENTRIES', entries: Array<EntryModel> }
    | { type: 'ADD_NEW_EMPTY_ENTRY' }

export const initialState: AppState = {
    entries: [emptyEntry()],
};

export const saveEntry = (dispatch: React.Dispatch<Action>) => (entry: EntryModel) => {
    if (isSaveable(entry)) {
        if (entry.id === '') {
            dispatch({type: 'ADD_NEW_EMPTY_ENTRY'});
        }
        api.saveEntry(entry)
        .then(updatedEntry => dispatch(
            {type: 'UPDATE_ENTRY', entry: updatedEntry}
        ));
    } else if (entry.subject === '' && entry.id !== '') {
        api.removeEntry(entry);
        dispatch({type: 'REMOVE_ENTRY', entry});
    }
};

export function reducer(state: AppState, action: Action): AppState {
    switch (action.type) {
        case 'UPDATE_ENTRY':
            return {...state, entries: state.entries.map(entry => {
                if (entry.id === action.entry.id) {
                    return action.entry;
                }
                return entry;
            })};

        case 'REMOVE_ENTRY':
            return {
                ...state,
                entries: state.entries.filter(e => e.id !== action.entry.id)
            }

        case 'LOADED_ENTRIES':
            return {...state, entries: [emptyEntry(), ...action.entries]};

        case 'ADD_NEW_EMPTY_ENTRY':
            return {...state, entries: [emptyEntry(), ...state.entries]};
    }
};

