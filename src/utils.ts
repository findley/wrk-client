type Classes = {
    [K: string]: boolean
};

export function classList(classes: Classes) {
    return Object
        .entries(classes)
        .filter(entry => entry[1])
        .map(entry => entry[0])
        .join(' ');
};
